package com.project.test

import android.app.Application
import com.project.test.module.appModule
import com.project.test.module.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

/**
 * Main Application
 */
class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // start Koin context
        startKoin {
            androidContext(this@BaseApplication)
            androidLogger()
            modules(listOf(appModule, networkModule))
        }
    }
}
