package com.project.test.entities


data class MenuItem (
    val id: Int,
    val title: String,
    val imageUrl: String?)