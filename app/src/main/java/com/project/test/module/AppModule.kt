package com.project.test.module

import com.project.test.repository.HelloRepository
import com.project.test.repository.MainRepositoryImpl
import com.project.test.presentation.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    // single instance of HelloRepository
    single<HelloRepository> { MainRepositoryImpl() }


    // MyViewModel ViewModel
    viewModel { MainViewModel(get()) }
}