package com.project.test.presentation.main

import android.os.Bundle
import android.widget.Toast
import com.project.framework.core.BaseActivity
import com.project.framework.core.ViewDataBindingOwner
import com.project.framework.widget.LoadingView
import com.project.test.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity :
    BaseActivity(),
    MainView {

    override fun getViewLayoutResId(): Int {
        return R.layout.activity_main
    }

    // inject ViewModel
    private val viewModel: MainViewModel by viewModel()

    override var retryListener: LoadingView.OnRetryListener = object : LoadingView.OnRetryListener {
        override fun onRetry() {
            viewModel.getMenuFromApi()
        }
    }

    private var doubleBackPressed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initUI()

        txt_test.text = viewModel.sayHello()
        txt_a.text = viewModel.bTextA.get()

        viewModel.getMenuFromApi()
        observeMenu()
    }

    private fun initUI() {
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(false)
            it.setHomeButtonEnabled(false)
        }


    }

    private fun observeMenu() {
        observeData(viewModel.menu) { result ->
            result?.menu?.let {
//                listAdapter.setData(it)
            }
        }
    }


    override fun onBackPressed() {
        if (doubleBackPressed) {
            super.onBackPressed()
            return
        }
        this.doubleBackPressed = true
        Toast.makeText(this, getString(
            R.string.msg_press_back_to_exit),
            Toast.LENGTH_SHORT).show()

        GlobalScope.launch(Dispatchers.Main) {
            delay(2000)
            doubleBackPressed = false
        }
    }
}
