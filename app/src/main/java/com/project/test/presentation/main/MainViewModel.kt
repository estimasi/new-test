package com.project.test.presentation.main

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.project.framework.core.BaseViewModel
import com.project.framework.core.NetworkState
import com.project.test.entities.Menu
import com.project.test.repository.HelloRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MainViewModel(private val helloRepository: HelloRepository) : BaseViewModel() {

    val showLoadingView = ObservableField(true)
    var bTextA = ObservableField("Test A")

    fun sayHello() = "${helloRepository.giveHello()} from $this"

    var menu: MutableLiveData<Menu> = MutableLiveData()

    fun getMenuFromApi() {
        networkState.value = NetworkState.LOADING

        GlobalScope.launch(Dispatchers.Main) {
            val request = helloRepository.getMenuAsync()
            try {
                val response = request.await()

                menu.value = response.body()!!

                if (response.body()!!.menu.isEmpty()) {
                    networkState.value = NetworkState.EMPTY
                } else {
                    networkState.value = NetworkState.LOADED
                }
            } catch (e: Exception) {
                networkState.value = NetworkState.error(e)
            }
        }
    }
}