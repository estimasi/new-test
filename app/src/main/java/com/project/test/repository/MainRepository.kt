package com.project.test.repository

import com.project.test.entities.Menu
import com.project.test.module.RetrofitFactory
import kotlinx.coroutines.Deferred
import retrofit2.Response

/**
 * Repository to provide a "Hello" data
 */

interface HelloRepository {
    fun giveHello(): String

    fun getMenuAsync(): Deferred<Response<Menu>>
}

class MainRepositoryImpl() : HelloRepository {
    override fun giveHello() = "Hello Koin"

    override fun getMenuAsync() = RetrofitFactory.createService().getMenuAsync()
}