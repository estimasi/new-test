package com.project.framework.core


interface ViewModelOwner<T : BaseViewModel> {
    val viewModel: T
}
